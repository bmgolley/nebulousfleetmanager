from kivymd.app import MDApp
from kivymd.uix.label import MDLabel
from kivymd.uix.list import OneLineListItem, ThreeLineListItem
from kivymd.uix.screen import MDScreen

from nfm import Fleet, Ship, ShipComponent, ShipSocket


class NFMScreen(MDScreen):
    
    prev_screen: str = None
    next_screen: str = None
    
    def on_touch_move(self, touch):
        if touch.x - touch.ox > min_swipe_length:
            self.go_back()
        elif touch.ox - touch.x > min_swipe_length:
            self.go_forwards()
    
    def go_back(self, *args):
        if self.prev_screen:
            app.open_screen(self.prev_screen, 'right')
    
    def go_forwards(self, *args):
        if self.next_screen:
            app.open_screen(self.next_screen, 'left')


class ListSeparator(OneLineListItem):
    pass


class NFMLabel(MDLabel):
    pass


class LabelWithLine(NFMLabel):
    pass


app: MDApp = None
fleet: Fleet = None
ship: Ship = None
ship_widget: ThreeLineListItem = None
socket: ShipSocket = None
socket_widget: ThreeLineListItem = None
comp: ShipComponent = None

min_swipe_length: int = 200