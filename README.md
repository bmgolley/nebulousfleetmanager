# Nebulous Fleet Manager

## Description
An Android app to allow viewing/creating/changing fleet and ship templates for the video game [NEBULOUS: Fleet Command](https://store.steampowered.com/app/887570/NEBULOUS_Fleet_Command/).

## Installation
Download the .apk and install on an Android device. This will require allowing "Installing from Unknown Sources."

## Building
Setup Buildozer per [their instructions](https://buildozer.readthedocs.io/en/latest/installation.html).
This apps uses features in the deveolpment version of KivyMD, so it will require cloning [the master branch](https://github.com/kivymd/KivyMD), then setting the requirements.source.kivymd property in the buildozer.spec file to point to that directory.
After that, just run ```buildozer android debug```