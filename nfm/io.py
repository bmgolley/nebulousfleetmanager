import json
import re
from pathlib import Path
from dicttoxml import dicttoxml
from xml.dom.minidom import Document, parseString

def to_xml(outdict: dict, filepath: str = None, fleet: bool = False) -> None:
    
    def _listnames(l: str) -> str:
        if l == 'Ships':
            return 'Ship'
        elif l == 'SocketMap':
            return 'HullSocket'
        elif l == 'MissileLoad' or l == 'Load':
            return 'MagSaveData'
        elif l == 'WeaponGroups':
            return 'WepGroup'
        elif l == 'MemberKeys':
            return 'string'
        else:
            return f'{l}_list'

    rootnode = 'Fleet' if fleet else 'Ship'
    xml = dicttoxml(outdict, attr_type=False,
                    custom_root=rootnode, item_func=_listnames)
    formatting = [
        # Add xml namespaces
        (fr'<({rootnode})>', r'<\1 xmlns:xsd='\
            r'"http://www.w3.org/2001/XMLSchema" xmlns:xsi='\
            r'"http://www.w3.org/2001/XMLSchema-instance">'),
        # Add namespace reference
        (r'<SaveID/>', r'<SaveID xsi:nil="true" />'),
        # Add missing namespace references to the parent node.
        # This is given in the format <parent><_name-space>value</_name-space>
        # Converted to <parent name:space="value"> 
        (r'<(\w+?)>\s*?<_(?P<t>(\w+?)-(\w+?))>(\w+?)</_(?P=t)>', 
            r'<\1 \3:\4="\5">'),
        # Moves weapon group name from a separate tag to a property of the 
        # weapon group tag.
        (r'<WepGroup>\s*?<Name>(.+?)</Name>',
            r'<WepGroup Name="\1">'),
        # Adds a space before the close of a tag with no value.
        (r'(?<!\s)/>', r' />')
    ]
    doc: Document = parseString(xml)
    outstr: str = doc.toprettyxml(indent='  ')
    doc.unlink()
    for f in formatting:
        outstr = re.sub(*f, outstr)
    with open(filepath, 'w') as outfile:
        outfile.write(outstr)

def _loaddata(filename: str, *keys) -> dict:
    def _listtotuple(_dict: dict) -> dict:
        for k, v in _dict.items():
            if k in keys and type(v) is list:
                _dict[k] = tuple(v)
            else:
                if type(v) is dict:
                    _dict[k] = _listtotuple(v)
        return _dict

    with open(f'{Path(__file__).parent}/{filename}') as f:
        d =  json.load(f)
    if keys:
        return _listtotuple(d)
    else:
        return d
    

ship_data = _loaddata('shipdata.json', 'size')
ship_classes = [c for c in ship_data.keys()]
ship_component_data = _loaddata('shipcomponentdata.json', 'size')

_componentslots: set = {c['slot'] for c in ship_component_data.values()}
components_by_slot = {
    t: {
        n: c for n, c in ship_component_data.items() if c['slot'] == t
    } for t in _componentslots
}

_ammotypes: set = {
    m['type'] for m in components_by_slot['ammo'].values()
}
ammo_by_type = {
    t: {
        n: c for n, c in components_by_slot['ammo'].items() if c['type'] == t
    } for t in sorted(_ammotypes)
}