import re
import xmltodict
from copy import deepcopy
from .io import to_xml
from .ship import Ship


class Fleet:
    """A Nebulous: Fleet Command fleet. Contains fleet information as well as owned ships."""
    err: str = None
    def load(filepath: str):
        """Imports a fleet from a .fleet file."""
        try:
            with open(filepath) as f:
                x = f.read()
        except Exception as ex:
            Fleet.err = str(ex)
            return None
        fleetxml = xmltodict.parse(x)
        fleetdict = fleetxml['Fleet']
        try:
            loadedfleet = Fleet(fleetdict['Name'])
        except Exception as ex:
            Fleet.err = str(ex)
            return None
        loadedfleet.filepath = filepath
        loadedfleet.description = fleetdict.get('Description')
        try:
            if type(shiplist:=fleetdict['Ships']['Ship']) is not list:
                shiplist = [shiplist]
            loadedfleet.ships  = {ship['Name']: Ship(**{k.lower(): v 
                                                        for k,v in ship.items()}
                                                    ) for ship in shiplist}
            for s in loadedfleet.ships.values():
                s.register_fleet(loadedfleet)
        except Exception as ex:
            Fleet.err = str(ex)
            return None
        else:
            return loadedfleet
        
    def __init__(self, name: str) -> None:
        """Create a new fleet."""
        self.name = name if name else "New fleet"
        """Fleet name"""
        self._version = 2
        """Unknown purpose. Always '2'"""
        self.description: str = None
        self._faction = "Stock/Alliance"
        """
        Until introduction of new factions, 
        all fleets will be in the 'Alliance' faction.
        """
        self.ships: dict = {}
        """All ships in the fleet, referenced by name."""
        self.filepath = None
        """Defualt filepath for saving to a .fleet file."""
    
    def get_ship(self, ship_name: str) -> Ship:
        """Returns a ship given its name."""
        ship = self.ships.get(ship_name)
        if not ship:
            self.ships = {s['name']: s for s in self.ships.values()}
            ship = self.ships[ship_name]
        return ship

    def ship_classes(self) -> dict:
        """Returns a dict of all ships and their classes."""
        return {n: s.hull_type for n, s in self.ships.items()}
        
    def add_ship(self, new_ship: Ship) -> None:
        """Add a new ship from a Ship object."""
        new_ship = self._check_ship_name(new_ship)
        self.ships[new_ship.name] = new_ship
        new_ship.register_fleet(self)
    
    def remove_ship(self, ship_name: str) -> None:
        """Removes the ship with the given name."""
        try:
            self.ships.get(ship_name).deregister_fleet(self)
            del self.ships[ship_name]
        except:
            pass
    
    def new_ship(self, hull_type: str, ship_name: str=None) -> Ship:
        """
        Create a blank ship given a hull type.
        """
        s = Ship(hull_type)
        if ship_name:
            s.name = ship_name
        else:
            # unnamedcount = 1
            if "cruiser" in s.hull_size:
                hs = s.hull_size.split("cruiser")[0]
                size = f"{hs.title()} Cruiser"
            else:
                size = s.hull_size.title()
            s.name = f'{size} 1'
            s = self._check_ship_name(s)
            # while not s.name:
            #     if (n:=f'{size} {unnamedcount}') not in self.ships:
            #         s.name = n
            #     else:
            #         unnamedcount += 1
        self.add_ship(s)
        return s
    
    def copy_ship(self, ship_name: str) -> Ship:
        """Duplicates an already  exisitng ship."""
        og_ship = self.get_ship(ship_name)
        new_ship = deepcopy(og_ship)
        new_ship = self._check_ship_name(new_ship, copy=True)
        #  = re.search('\d+$', new_ship.name)
        # if new_ship_n:=re.search('\d+$', new_ship.name):
        #     n = False
        #     i = int(new_ship_n.group()) + 1
        #     f_name = lambda x:  re.sub(r'(\d+)$',str(x),new_ship.name)
        #     name = f_name(i)
        #     while not n:
        #         if name not in self.ships:
        #             new_ship.name = name
        #             n = True
        #         else:
        #             i += 1
        #             name = f_name(i)
        # elif new_ship.name.endswith("Copy"):
        #     new_ship.name = f"{new_ship.name} 1"
        # else:
        #     new_ship.name = f"{new_ship.name} Copy"
        new_ship.number += 1
        self.add_ship(new_ship)
        return new_ship

    def cost(self) -> int:
        """Total point cost of every ship in the fleet."""
        cost = 0
        for s in self.ships.values():
            cost += s.cost()
        return cost
    
    def save(self, filepath: str = None) -> None:
        """
        Exports the fleet to a .fleet file. If this was originally imported 
        from a file, it will default to saving to that file.
        """
        if not filepath and self.filepath:
            filepath = self.filepath
        outdict = {
            'Name': self.name,
            'Version': self._version,
            'TotalPoints': self.cost(),
            'FactionKey': self._faction
        }
        if d:=self.description:
            outdict['Description'] = d
        outdict['Ships'] = list(
            ship.save(fleet=True)
            for ship in self.ships.values()
        )
        to_xml(outdict, filepath, True)
        
    def _check_ship_name(self, new_ship: Ship, copy: bool = False):
        if num:=re.search('\d+$', new_ship.name):
            n = False
            i = int(num.group())
            f_name = lambda x:  re.sub(r'(\d+)$',str(x),new_ship.name)
            # name = new_ship.name
            while not n:
                if new_ship.name not in self.ships:
                    # new_ship.name = name
                    n = True
                else:
                    i += 1
                    new_ship.name = f_name(i)
        elif copy:
            if new_ship.name.endswith("Copy"):
                new_ship.name = f"{new_ship.name} 1"
            else:
                new_ship.name = f"{new_ship.name} Copy"
        return new_ship
 